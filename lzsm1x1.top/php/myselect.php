<?php
header('Access-Control-Allow-Origin:*');// *代表允许任何网址请求
include_once("conn.php");

$cmd = $_POST['cmd'];
$data = $_POST['data'];//"user1";


if($cmd  == "select")
{
    $sql = "SELECT * FROM `sensor` WHERE 1"; //选择表
    if (!$result = $dbconn->query($sql)) {
        echo "{'err':'dabase'}";//数据库选择错误
        exit;
    }
    if ($result->num_rows === 0) {
        echo "{'err':'noresult'}";
        exit;
    }
    $arr=[];
    while($row = $result->fetch_assoc()) {
        $arr['id'][]=$row['id'];    //遍历表的字段
        $arr['temp'][]=$row['temp'];
        $arr['humi'][]=$row['humi'];
        $arr['lux'][]=$row['lux'];
    }
    $str1 = json_encode($arr);

    echo $str1;
}

?>