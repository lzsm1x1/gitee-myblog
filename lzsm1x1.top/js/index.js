jQuery(document).ready(function() {		//类似int main
	$('.btn').on('click', function (e) {	//监听button
		console.log("You click Login Button")	//printf
		var name = $("#uname").val().trim();	//定义变量name .val()获取输入内容 .trim()去除字符串的头尾空格
		var paswd = $("#pwd").val().trim();
		if (name == "" || paswd == "") {
			alert("输入不能为空！");
			return;
		}
		var postData = {
			name:name,
			passwd:paswd
		}
		$.post("php/ckpswd.php", postData,
		    function (data, status) {
		        console.log("返回值:", data, "状态：", status);
		        if (status == "success") {
		            backData = eval('(' + data + ')');//字符串转对象
		            if (backData.err) {
		                var res = backData.err;
		                if (res == "password") {
		                    alert("密码错误!");
		                } else if (res == "nouser") {
							alert("没有该用户!");
		                } else {
							alert("数据表错误!");
		                }
		            } else if (backData.success) {
		                var res = backData.success;
		                if (res == "login") {//修改密码成功
							window.location.href="htm/main.html?name="+encodeURI(postData.name);
		                }

		            }

		        } else if (status == "error") {
		            alert('数据库访问错误！')
		        }
		    });

	});
});
